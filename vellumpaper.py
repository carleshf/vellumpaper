# -*- coding: utf-8 -*-

import os
import toml
import shutil
import argparse
import markdown2

from functools import reduce
from collections import defaultdict

def read_conf_file( filename ):
	""" Function to read and load a TOML configuration file.

	Args:
	    filename (str): Path until a TOML configuration file.

	Returns:
		dict: TOML configuration file loaded as a dictionary.
	"""
	if os.path.isfile( filename ):
		config = toml.load( filename )
	else:
		raise Exception( "Configuration file '" + filename + "' does not exists." )
	return( config )

def read_markdown_file( filename ):
	""" Function to read a markdown file descigned to be a "post" in "vellupaper".

	The markdown files for vellumpaper have two sections: a header, and a body. The hedaer is delimited by a starting line with "+++" and it is closed with a line with "+++". The body is a standard markdown text.

	Args:
		filename (str): Path until a vellupaper makdown file.

	Returns:
		dict: having a header and a body.
	"""
	with open( filename, "r" ) as fh:
		content = fh.readlines()
	if content[ 0 ].startswith( "+++" ):
		header = []
		ii = 1
		while True:
			if content[ ii ].startswith( "+++" ):
				break
			header.append( content[ ii ] )
			ii += 1
		content = { "header": parse_markdown_header( header ), \
			"body": reduce( lambda x, y: x + y, content[ ( ii + 1 ):len( content ) ] ) }
	else:
		raise Exception( "Markdown file '" + filename + "' does not have header." )
	return content

def parse_markdown_header( string_header ):
	""" Function to parse a vellumpaper makrdown header.

	Args:
		string_header (list): List of strings. Each string is a propery of the header.

	Returns:
		dict: Having the pairs of key-value of the header.
	"""
	dict_header = defaultdict( str )
	for line in string_header:
		line = [ x.strip() for x in line.split( ":" ) ]
		dict_header[ line[ 0 ].lower() ] = line[ 1 ]
	return( dict_header )

def read_templates( list_templates, path ):
	""" Function to load HTML templates

	Args:
		list_templates (dict): dictionary of names/files to templates to be loaded.
		path (string): path where the HTML files (templates) are located.

	Returns:
		dict: Having name and HTML content.
	"""
	html_parts = {}
	for part in list_templates.keys():
		part = part.lower()
		with open( os.path.join( path, list_templates[ part ] ), "r" ) as fh:
			html_parts[ part ] = reduce( lambda x, y: x + y, fh.readlines() )
	return( html_parts )

class WebSite:
	"""Class encapsulating all web site information.

	Attributes:
		in_path (str): Input path pointing to a "conent" folder, having the main TOML configuration file.
		out_path (str): Output path where all the HTML content will be generated.
		template_path (str): Path where the HTML templates are located (it can takes None value).
		static_path (str): Path where the static elements used in the web site are located (it can takes None value).
		config (dict): Main configuration of the web site loaded from main TOML configuration file.
		sections
		templates
		has_templates
	"""
	def __init__( self, in_path, out_path, template_path, static_path  ):
		""" Class creation method.

		Args:
			in_path (str): Input path pointing to a "conent" folder, having the main TOML configuration file.
			out_path (str): Output path where all the HTML content will be generated.
			template_path (str): Path where the HTML templates are located (it can takes None value).
			static_path (str): Path where the static elements used in the web site are located (it can takes None value).
		"""
		self.in_path = in_path
		self.out_path = out_path
		self.template_path = template_path
		self.static_path = static_path
		self.config = None
		self.sections = []
		self.templates = {}
		self._read_conf()
		self.has_templates = "templates" in self.config.keys()
		self._nav_bar = ""

	def _read_conf( self ):
		""" Load main TOML configuration file.

		The method load the main TOML configuration file from input path (self.in_path). 

		Note:
			This method is called in the constructor of the class so user shoould not use it.
		"""
		self.config = read_conf_file( os.path.join( self.in_path, "config.toml" ) )
		print( "Loaded main config file" )
		print( "  . base: '" + self.config[ "info" ][ "base" ] + "'" )
		print( "  . sections: ", self.config[ "structure" ][ "sections" ] )
		if "templates" in self.config.keys():
			print( "  . templates: " + reduce( lambda x, y: x + ", " + y, self.config[ "templates" ] ) )
		else:
			print( "  . templates:  not provided"  )

	def read_sections( self ):
		""" Read the internal TOML file of each section and loads all the md files.
		"""
		for section_folder in self.config[ "structure" ][ "sections" ]:
			sec = WebSection( os.path.join( self.in_path, section_folder ) )
			sec.read_conf( section_folder )
			sec.read_files( section_folder )
			self.sections.append( sec )

	def read_templates( self ):
		""" Given a path with templates, it reads the HTML. If templates are not provided it creates the default ones.
		"""
		if not self.has_templates:
			self.templates[ "landing" ]  = "<div><h1>//%title%//</h1></div><div><ul>//%sections%//</ul></div>"
			self.templates[ "navitem" ]  = "<li><a href=\"//%url%//\">//%sectitle%//</a></li>"
			self.templates[ "header" ]   = "<!DOCTYPE html><html><head><title>//%title%//</title></head><base href=\"//%base%//\"/><body>"
			self.templates[ "footer" ]   = "</body></html>"
			self.templates[ "content" ]  = "<div><h2>//%contenttitle%//</h2><br/>//%content%//</div>"
			self.templates[ "listpage" ] = "<div><h2>//%sectitle%//</h2><br/></div><div><ul>//%content%//</ul></div>"
			self.templates[ "listitem" ] = "<li>(//%date%//) //%contenttitle%//<br/><a href=\"//%url%//\">Details</a></li>"
		else:
			if self.template_path is None:
				raise IOError( "Templates were specified in configuration files but no path for templaes was provided" )
			self.templates = read_templates( self.config[ "templates" ], self.template_path )
			for sec in self.sections:
				sec.read_templates( self.template_path )
		self._create_nav_bar()
		self._replace_tags()

	def _replace_tags( self ):
		"""Replace all the tags in the templates (tag: //%word%//) with its value from configuration.
		"""
		for tpl in self.templates.keys():
			for keyword in self.config[ "info" ].keys():
				self.templates[ tpl ] = self.templates[ tpl ].replace( \
					"//%" + keyword + "%//", self.config[ "info" ][ keyword ] )
			self.templates[ tpl ] = self.templates[ tpl ].replace( "//%sections%//", self._nav_bar )

	def _create_nav_bar( self ):
		"""Creates the HTML version of the navigation bar according to the main TOML configuration file.
		"""
		self._nav_bar = []
		for sec in self.sections:
			self._nav_bar.append( self.templates[ "navitem" ].replace( \
				"//%sectitle%//", sec.config[ "info" ][ "sectitle"] ).replace( \
				"//%url%//", sec.out_path ) )
		self._nav_bar = reduce( lambda x, y: x + y, self._nav_bar )

	def write_html( self ):
		if "landing" in self.templates.keys():
			print( "Writing landing page" )
			self._write_landing()
			self._write_sections()
		else:
			raise Exception( "No landing page provided" )

	def _write_landing( self ):
		""" Writes the HTML version of the landing page - if provided in the templates.
		"""
		if "landing" in self.templates:
			with open( os.path.join( self.out_path, "index.html" ), "w" ) as fh:
				fh.writelines( self.templates[ "landing" ] )

	def _write_sections( self ):
		""" For each section it creates the according HTML files.
		"""
		for sec in self.sections:
			sec.write_html( self.out_path, self.templates )

	def move_static( self ):
		if not self.static_path is None:
			print( "Copying static folder" )
			for file_or_folder in os.listdir( self.static_path ):
				shutil.copytree( os.path.join( self.static_path, file_or_folder ), \
					os.path.join( self.out_path, file_or_folder ) )

class WebSection:

	def __init__( self, in_path ):
		self.in_path = in_path
		self.out_path = ""
		self.files = []
		self.has_templates = False

	def read_conf( self, section_folder ):
		""" Load main TOML configuration file.

		The method load the main TOML configuration file from input path (self.in_path). 

		Note:
			This method is called in the constructor of the class so user shoould not use it.
		"""
		self.config = read_conf_file( os.path.join( self.in_path, "config.toml" ) )
		if not "order" in self.config.keys():
			self.config[ "info" ][ "order" ] = "descending"
		self.out_path = section_folder + "/index.html"
		print( "Loaded main config file for '" + section_folder + "'" )
		print( "  . sectitle: '" + self.config[ "info" ][ "sectitle" ] + "'" )
		print( "  . type: '" + self.config[ "info" ][ "type" ] + "'" )
		print( "  . order: '" + self.config[ "info" ][ "order" ] + "'" )
		self.has_templates = "templates" in self.config.keys()
		if self.has_templates:
			print( "  . templates: " + reduce( lambda x, y: x + ", " + y, self.config[ "templates" ] ) )
		else:
			print( "  . templates:  not provided"  )

	def read_files( self, section_folder ):
		""" Loads all the markdown files in the section's folder.

		Note:
			It will try to read any file that is not 'config.toml' not takeing into acount file extensions.
		"""
		for filename in os.listdir( self.in_path ):
			if filename != "config.toml":
				self.files.append( read_markdown_file( os.path.join( self.in_path, filename ) ) )
		def order( ff ):
			if "date" in ff[ "header" ]:
				date = [ int( ii ) for ii in ff[ "header" ][ "date" ].split( "-" ) ]
				return date[ 0 ] * 365 + date[ 1 ] * 30 + date[ 2 ]
			else:
				return 0
		if self.config[ "info" ][ "order" ] == "descending":
			self.files = sorted( self.files, key = order, reverse = True )
		else:
			self.files = sorted( self.files, key = order, reverse = False )

		print( "Loaded " + str( len( self.files ) ) + " file(s) for '" + section_folder + "'" )

	def read_templates( self, template_path ):
		if self.has_templates:
			self.templates = read_templates( self.config[ "templates" ], template_path )
		else:
			self.templates = defaultdict( str )

	def write_html( self, out_path, templates ):
		utemplates = { **templates, **self.templates }
		print( "Writing section '" + self.config[ "info" ][ "sectitle" ] + "'" )
		if self.config[ "info" ][ "type" ] == "list":
			files_list = self._write_files_html( out_path, utemplates )
			self._write_index_html( out_path, files_list, utemplates )
		if self.config[ "info" ][ "type" ] == "front":
			self._write_front_html( out_path, utemplates )

	def _write_index_html( self, out_path, files_list, templates ):
		os.makedirs( os.path.join( out_path, os.path.dirname( self.out_path ) ), exist_ok = True )
		if "listpage" in templates.keys() and "listitem" in templates.keys():
			html = ""
			if "header" in templates.keys():
				html += templates[ "header" ]
			html += templates[ "listpage" ].replace( "//%sectitle%//", self.config[ "info" ][ "sectitle" ] )
			item_list = []
			for file in files_list:
				file_html = templates[ "listitem" ].replace( "//%contenttitle%//", file[ "title" ] ).replace( \
					"//%date%//", file[ "date" ] ).replace( \
					"//%url%//", file[ "url" ] )
				for tag in file[ "header" ].keys():
					file_html = file_html.replace( "//%" + tag + "%//", file[ "header" ][ tag ] )
				item_list.append( file_html )
			html = html.replace( "//%content%//", reduce( lambda x, y: x + y, item_list ) )
			if "footer" in templates.keys():
				html += templates[ "footer" ]
			with open( os.path.join( out_path, self.out_path ), "w" ) as fh:
				fh.writelines( html )
			print( "Written index file for section '" + self.config[ "info" ][ "sectitle" ] + "'" )
		else:
			print( "Index file for section '" + self.config[ "info" ][ "sectitle" ] + "' could not be written. No 'listpage' template provided." )
				

	def _write_files_html ( self, out_path, templates ):
		write_path = os.path.join( out_path, os.path.dirname( self.out_path ) )
		os.makedirs( write_path, exist_ok = True )

		html_urls = []
		if "content" in templates.keys():
			for content in self.files:
				html_file_name = content[ "header" ][ "slug" ].replace( " ", "-" ) + ".html"
				file_name = os.path.join( write_path, html_file_name )
				html_content = markdown2.markdown( content[ "body" ], extras = [ "tables" ] )

				html = ""
				if "header" in templates.keys():
					html += templates[ "header" ]
				html += templates[ "content" ].replace( \
					"//%contenttitle%//", content[ "header" ][ "title" ] ).replace( \
					"//%content%//", html_content )
				for tag in content[ "header" ]:
					html.replace( "//%" + tag + "%//", content[ "header" ][ tag ] )
				if "footer" in templates.keys():
					html += templates[ "footer" ]

				with open( file_name, "w" ) as fh:
					fh.writelines( html )

				html_urls.append( { "title": content[ "header" ][ "title" ], \
					"date": content[ "header" ][ "date" ], \
					"url": os.path.dirname( self.out_path ) + "/" + html_file_name,
					"header": content[ "header" ] } )
		else:
			raise Exception( "No 'content' templates was provided." )

		print( "Writing " + str( len( html_urls ) ) + " files for section '" + self.config[ "info" ][ "sectitle" ] + "'" )
		return( html_urls )

	def _write_front_html( self, out_path, templates ):
		os.makedirs( os.path.join( out_path, os.path.dirname( self.out_path ) ), exist_ok = True )
		if "content" in templates.keys():
			if len( self.files ) != 1:
				raise Exception( "Front section with " + len( self.files ) + " files." )
			
			content = self.files[ 0 ]
			html_content = markdown2.markdown( content[ "body" ], extras = [ "tables" ] )

			html = ""
			if "header" in templates.keys():
				html += templates[ "header" ]
			html += templates[ "content" ].replace( \
				"//%contenttitle%//", content[ "header" ][ "title" ] ).replace( \
				"//%content%//", html_content )
			for tag in content[ "header" ]:
				html.replace( "//%" + tag + "%//", content[ "header" ][ tag ] )
			if "footer" in templates.keys():
				html += templates[ "footer" ]

			with open( os.path.join( out_path, self.out_path ), "w" ) as fh:
				fh.writelines( html )
			print( "Written front file for section '" + self.config[ "info" ][ "sectitle" ] + "'" )
		else:
			raise Exception( "No 'content' templates was provided." )


if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument( "-i", "--in", required = True, help = "Folder to be processed (location of main 'config.toml')" )
	ap.add_argument( "-o", "--out", required = True, help = "Folder where the output will be placed. If it exists, it will be previously deleted." )
	ap.add_argument( "-t", "--template", required = False, default = None, help = "Folder containing the templates to be used." )
	ap.add_argument( "-s", "--static", required = False, default = None, help = "Folder containing the static resources required by the generated site." )
	args = vars(ap.parse_args())

	if os.path.exists( args [ "out" ] ):
		shutil.rmtree( args [ "out" ] )
	os.makedirs( args [ "out" ] )

	site = WebSite( args[ "in" ], args [ "out" ], args[ "template" ], args[ "static" ] )
	site.read_sections()
	site.read_templates()
	site.write_html()
	site.move_static()

# python vellumpaper2.py -i "C:/Users/Carles Hernandez/Documents/Projects/web/content" -o "C:/Users/Carles Hernandez/Documents/Projects/web/vellumpaper" -t "C:/Users/Carles Hernandez/Documents/Projects/web/templates" -s "C:/Users/Carles Hernandez/Documents/Projects/web/static"